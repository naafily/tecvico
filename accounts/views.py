from django.contrib.auth.views import LoginView, LogoutView
from django.contrib.messages.views import SuccessMessageMixin
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.views.generic import CreateView

from .forms import SignUpForm
from .models import Account


class SignUpView(SuccessMessageMixin, CreateView):
    form_class = SignUpForm
    template_name = 'accounts/signup.html'
    success_message = " your account was created successfully"

    def form_valid(self, form):
        obj = form.save()
        account = Account(user=obj)
        account.save()
        return HttpResponseRedirect(reverse('login'))


class LoginViewClass(SuccessMessageMixin, LoginView):
    success_message = " you have logged in successfully"
    template_name = 'accounts/login.html'

    def get_success_url(self):
        return reverse('main-page')


class LogoutViewClass(SuccessMessageMixin, LogoutView):
    success_message = " you have logged out successfully"

    def dispatch(self, request, *args, **kwargs):
        response = super().dispatch(request, *args, **kwargs)
        response.delete_cookie('second')
        response.delete_cookie('minute')
        response.delete_cookie('hour')
        return response
