from django.urls import path, include

from .views import SignUpView, LoginViewClass, LogoutViewClass

urlpatterns = [
    path('signup/', SignUpView.as_view(), name='signup'),
    path('login/', LoginViewClass.as_view(), name='login'),
    path('logout/', LogoutViewClass.as_view(), name='logout'),
    path('', include('django.contrib.auth.urls')),
]
