from django.urls import path

from . import views

urlpatterns = [
    path("", views.main_page, name='main-page'),
    path("create-task/", views.create_task, name='create-task'),

]
