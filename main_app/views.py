from django.contrib.auth.decorators import login_required
from django.http import HttpResponse
from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt

from .models import Task, Account
from .utility import format_time


@login_required()
def main_page(request):
    tasks = Task.objects.filter(account__user=request.user)
    return render(request, 'main_app/home.html', {'tasks': tasks})


@csrf_exempt
def create_task(request):
    name = request.POST.get('name', 'Task')
    time = request.POST.get('time')
    account = Account.objects.get(user=request.user)
    task = Task.objects.create(name=name, time=format_time(int(time)), account=account)
    if task:
        return HttpResponse(status=201)
    return HttpResponse(status=400)
