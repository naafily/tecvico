from django.db import models
from accounts.models import Account


class Task(models.Model):
    name = models.CharField(max_length=100)
    time = models.TextField()
    account = models.ForeignKey(Account, on_delete=models.CASCADE, related_name='tasks')

    class Meta:
        ordering = ('-time',)
